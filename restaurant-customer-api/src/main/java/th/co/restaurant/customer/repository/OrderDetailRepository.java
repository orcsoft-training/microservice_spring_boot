package th.co.restaurant.customer.repository;

import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;
import th.co.restaurant.customer.entity.OrderDetailEntity;

@Repository
public interface OrderDetailRepository extends CrudRepository<OrderDetailEntity, Integer> {

}
