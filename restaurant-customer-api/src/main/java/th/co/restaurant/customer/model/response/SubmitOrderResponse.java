package th.co.restaurant.customer.model.response;

public class SubmitOrderResponse extends ResponseMessage {
    public SubmitOrderResponse(Integer orderId) {
        this.setCode("000");
        this.setDescription("Order id: " + orderId + " created.");
    }
}
