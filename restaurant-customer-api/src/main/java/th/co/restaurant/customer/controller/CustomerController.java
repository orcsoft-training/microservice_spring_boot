package th.co.restaurant.customer.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import th.co.restaurant.customer.model.exception.MyException;
import th.co.restaurant.customer.model.request.SubmitOrderRequest;
import th.co.restaurant.customer.model.response.GetMenuResponse;
import th.co.restaurant.customer.model.response.SubmitOrderResponse;
import th.co.restaurant.customer.service.GetMenuService;
import th.co.restaurant.customer.service.SubmitOrderService;
import th.co.restaurant.customer.service.ValidatorService;

@Validated
@RestController
public class CustomerController {

    @Autowired
    private SubmitOrderService submitOrderService;
    @Autowired
    private GetMenuService getMenuService;
    @Autowired
    private ValidatorService validatorService;

    @GetMapping(value = "getMenu")
    public ResponseEntity<GetMenuResponse> getMenu() {
        final GetMenuResponse menu = getMenuService.getMenu();
        return ResponseEntity.ok(menu);
    }

    @PostMapping(value = "summitOrder")
    public ResponseEntity<SubmitOrderResponse> submitOrder(@Valid @RequestBody SubmitOrderRequest submitOrderRequest) throws MyException {
        validatorService.submitOrder(submitOrderRequest);
        SubmitOrderResponse submitOrderResponse = submitOrderService.submitOrder(submitOrderRequest);

        return ResponseEntity.ok(submitOrderResponse);
    }

}
