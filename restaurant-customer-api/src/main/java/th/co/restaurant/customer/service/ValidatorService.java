package th.co.restaurant.customer.service;

import org.springframework.stereotype.Service;
import th.co.restaurant.customer.model.exception.MyException;
import th.co.restaurant.customer.model.request.SubmitOrderRequest;

@Service
public class ValidatorService {

    public void submitOrder(SubmitOrderRequest submitOrderRequest) throws MyException {
        if (!submitOrderRequest.getCustomerName().startsWith("AA")) {
            throw new MyException("001", "Customer id must start with \"AA\"");
        }

        if (submitOrderRequest.getOrderMenus() != null && submitOrderRequest.getOrderMenus().size() > 5) {
            throw new MyException("001", "OrderMenus maximise = 5");
        }

    }
}
