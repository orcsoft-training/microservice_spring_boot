package th.co.restaurant.customer.model;

import java.util.ArrayList;
import java.util.List;

import th.co.restaurant.customer.entity.MenuEntity;

public class MenuList extends ArrayList<MenuEntity> {

	private static final long serialVersionUID = 1L;

	public MenuList(List<MenuEntity> data) {
        this.addAll(data);
    }

}
