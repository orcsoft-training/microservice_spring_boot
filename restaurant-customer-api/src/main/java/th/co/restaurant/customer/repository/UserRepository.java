package th.co.restaurant.customer.repository;

import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;
import th.co.restaurant.customer.entity.UserEntity;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Integer> {

}
