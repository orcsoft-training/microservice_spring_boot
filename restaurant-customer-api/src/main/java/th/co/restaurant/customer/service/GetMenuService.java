package th.co.restaurant.customer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import th.co.restaurant.customer.entity.MenuEntity;
import th.co.restaurant.customer.model.MenuList;
import th.co.restaurant.customer.model.response.GetMenuResponse;
import th.co.restaurant.customer.repository.MenuRepository;

@Service
public class GetMenuService {

    @Autowired
    private MenuRepository menuRepository;

    public GetMenuResponse getMenu() {
        List<MenuEntity> all = menuRepository.findAll();
        final GetMenuResponse getMenuResponse = new GetMenuResponse();
        getMenuResponse.setMenuList(new MenuList(all));
        return getMenuResponse;
    }
}
