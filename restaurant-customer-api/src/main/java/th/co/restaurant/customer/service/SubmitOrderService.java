package th.co.restaurant.customer.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import th.co.restaurant.customer.entity.MenuEntity;
import th.co.restaurant.customer.entity.OrderDetailEntity;
import th.co.restaurant.customer.entity.OrderEntity;
import th.co.restaurant.customer.model.OrderMenu;
import th.co.restaurant.customer.model.exception.MyException;
import th.co.restaurant.customer.model.request.SubmitOrderRequest;
import th.co.restaurant.customer.model.response.SubmitOrderResponse;
import th.co.restaurant.customer.repository.MenuRepository;
import th.co.restaurant.customer.repository.OrderDetailRepository;
import th.co.restaurant.customer.repository.OrderRepository;

@Service
public class SubmitOrderService {
	
	Logger log = LogManager.getLogger(SubmitOrderService.class);

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    public SubmitOrderResponse submitOrder(SubmitOrderRequest submitOrderRequest) throws MyException {
        OrderEntity orderEntity = submitOrderRequest.getOrderEntity();
        List<OrderDetailEntity> orderDetailEntities = validateMenuList(submitOrderRequest.getOrderMenus());

        try {
            // save order to table 'customer_order'
            orderRepository.save(orderEntity);
            // After 'save' entity must get PK to the field, then take it to update OrderDetailEntity list.
            orderDetailEntities.forEach(orderDetailEntity -> orderDetailEntity.setOrderId(orderEntity.getOrderId()));
            // save order detail to table 'customer_order_detail'
            orderDetailRepository.saveAll(orderDetailEntities);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            throw new MyException("999", "Process failed by somehow.");
        }

        log.info("Crete orderId: {} with {}details", orderEntity.getOrderId(), orderDetailEntities.size());
        return new SubmitOrderResponse(orderEntity.getOrderId());
    }


    // Validate and generate data in single loop.
    private List<OrderDetailEntity> validateMenuList(List<OrderMenu> orderMenus) throws MyException {
        List<OrderDetailEntity> orderDetailEntities = new ArrayList<>();
        for (OrderMenu orderMenu : orderMenus) {
            // Find all menu to make sure every single menu is valid.
            MenuEntity menuEntity = menuRepository.findById(orderMenu.getId()).orElseThrow(
                    () -> new MyException("002", "menu_id: " + orderMenu.getId() + " is undefined")
            );

            // Prepare OrderDetailEntity
            orderDetailEntities.add(new OrderDetailEntity(menuEntity.getMenuId(), 
            		menuEntity.getName(), orderMenu.getQuantity(), menuEntity.getPrice(), "A"));
        }

        return orderDetailEntities;
    }
}
