package th.co.restaurant.customer.constants;

public class RestaurantConstants {

	public static enum OrderStatusConstants {
		OPEN_ORDER, OPEN_BILLS, SHIPPING, CANCEL_ORDER, COMPLETED
	}
	
}
