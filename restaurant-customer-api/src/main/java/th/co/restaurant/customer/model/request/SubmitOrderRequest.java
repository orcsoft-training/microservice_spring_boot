package th.co.restaurant.customer.model.request;

import java.util.List;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModelProperty;
import th.co.restaurant.customer.constants.RestaurantConstants;
import th.co.restaurant.customer.entity.OrderEntity;
import th.co.restaurant.customer.model.OrderMenu;

public class SubmitOrderRequest {
    @NotBlank
    @ApiModelProperty(example = "AA0001")
    private String customerName;

    @NotBlank
    @ApiModelProperty(example = "034341550-3")
    private String mobile;

    @NotBlank
    @ApiModelProperty(example = "ku.kps@orcsoft.co.th")
    private String email;

    @NotBlank
    @ApiModelProperty(example = "no.1, village 6, Malai Maen Road, Kamphaeng Saen Sub-District, Kamphaeng Saen District, Nakhon Pathom Province 73140")
    private String deliveryAddress;

    private List<OrderMenu> orderMenus;
    
    public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public List<OrderMenu> getOrderMenus() {
		return orderMenus;
	}

	public void setOrderMenus(List<OrderMenu> orderMenus) {
		this.orderMenus = orderMenus;
	}

	@ApiModelProperty(hidden = true)
    public OrderEntity getOrderEntity() {
        return new OrderEntity(this.customerName, this.mobile, this.email, this.deliveryAddress, RestaurantConstants.OrderStatusConstants.OPEN_ORDER.name());
    }
}
