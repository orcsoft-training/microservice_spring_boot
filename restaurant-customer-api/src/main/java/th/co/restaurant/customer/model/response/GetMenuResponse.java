package th.co.restaurant.customer.model.response;

import th.co.restaurant.customer.model.MenuList;

public class GetMenuResponse extends ResponseMessage {

    public GetMenuResponse() {
        this.setCode("000");
        this.setDescription("Success");
    }

    private MenuList menuList;

	public MenuList getMenuList() {
		return menuList;
	}

	public void setMenuList(MenuList menuList) {
		this.menuList = menuList;
	}
    
    

}
