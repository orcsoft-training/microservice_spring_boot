package th.co.restaurant.customer.model;

import io.swagger.annotations.ApiModelProperty;


public class OrderMenu {
    @ApiModelProperty(example = "1305")
    Integer id;
    @ApiModelProperty(example = "3")
    Integer quantity;
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
    
    
}
