package th.co.restaurant.customer.repository;

import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;
import th.co.restaurant.customer.entity.OrderEntity;

@Repository
public interface OrderRepository extends CrudRepository<OrderEntity, Integer> {

}
