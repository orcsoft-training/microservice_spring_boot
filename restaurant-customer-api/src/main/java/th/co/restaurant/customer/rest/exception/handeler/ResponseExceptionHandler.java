package th.co.restaurant.customer.rest.exception.handeler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import th.co.restaurant.customer.model.response.ResponseMessageImpl;

@RestControllerAdvice
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String error = ex.getBindingResult().getFieldErrors().stream().map((FieldError e) -> e.getField() + ": " + e.getDefaultMessage()).reduce("", String::concat);
        error = ex.getBindingResult().getFieldErrors().stream().map((ObjectError e) -> e.getObjectName() + ": " + e.getDefaultMessage()).reduce(error, String::concat);

        final ResponseMessageImpl responseMessage = new ResponseMessageImpl();
        responseMessage.setCode(HttpStatus.BAD_REQUEST.toString());
        responseMessage.setDescription(error);
        return ResponseEntity.badRequest().body(responseMessage);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleAll(final Exception ex, final WebRequest request) {
        logger.info(ex.getClass().getName());
        logger.error("error", ex);
        final ResponseMessageImpl responseMessage = new ResponseMessageImpl();
        responseMessage.setCode(HttpStatus.BAD_REQUEST.toString());
        responseMessage.setDescription(ex.getMessage());
        return ResponseEntity.badRequest().body(responseMessage);
    }
}
